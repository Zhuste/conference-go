import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,
        "query": city + " " + state
    }
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_data(city, state):
    url = "https://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city + "," + state + "," + "840",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response_data = requests.get(url, params=params)
    content = json.loads(response_data.content)
    try:
        longitude = content[0]["lon"]
        latitude = content[0]["lat"]
    except (KeyError, IndexError):
        return None

    url_w = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
    }

    weather = requests.get(url_w, params)
    content = json.loads(weather.content)

    try:
        return {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
            }
    except (KeyError, IndexError):
        return None
