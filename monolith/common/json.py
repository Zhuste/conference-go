from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
#   if the object to decode is the same class as what's in the
        if isinstance(o, self.model):
#   model property, then
#     * create an empty dictionary that will hold the property names
#       as keys and the property values as values
            dict = {}
        #     * for each name in the properties list
            for p in self.properties:
        #   ^----self.properties refers to properties
        #       *in events>api_views.py - class ConferenceDetailEncoder
        #         * get the value of that property from the model instance
        #           given just the property name
                value = getattr(o, p)
                if p in self.encoders:
                    encoder = self.encoders[p]
                    value = encoder.default(value)
        #         * put it into the dictionary with that property name as
        #           the key
                dict[p] = value
        #       if o has the attribute get_api_url then add it's return value
        #       to Dict with the key "href"
            if hasattr(o, "get_api_url"):
                dict["href"] = o.get_api_url()
            dict.update(self.get_extra_data(o))
        #     * return the dictionary
            return dict
        #   otherwise,
        else:
            return super().default(o)
        #       return super().default(o)  # From the documentation

    def get_extra_data(self, o):
        return {}
